package stat

import (
	"fmt"
	"math"
	"net/http"

	"gitlab.com/mayachain/midgard/config"
	"gitlab.com/mayachain/midgard/internal/timeseries"
)

func cacaoPriceUSDForDepths(depths timeseries.DepthMap) float64 {
	ret := math.NaN()
	var maxdepth int64 = -1

	for _, pool := range config.Global.UsdPools {
		poolInfo, ok := depths[pool]
		if ok && maxdepth < poolInfo.RuneDepth {
			maxdepth = poolInfo.RuneDepth
			ret = poolInfo.CacaoPrice()
		}
	}
	return ret
}

// Returns the 1/price from the depest whitelisted pool.
func CacaoPriceUSD() float64 {
	return cacaoPriceUSDForDepths(timeseries.Latest.GetState().Pools)
}

func ServeUSDDebug(resp http.ResponseWriter, req *http.Request) {
	state := timeseries.Latest.GetState()
	for _, pool := range config.Global.UsdPools {
		poolInfo := state.PoolInfo(pool)
		if poolInfo == nil {
			fmt.Fprintf(resp, "%s - pool not found\n", pool)
		} else {
			depth := float64(poolInfo.RuneDepth) / 1e10
			cacaoPrice := poolInfo.CacaoPrice()
			fmt.Fprintf(resp, "%s - originalDepth: %d runeDepth: %.0f assetDepth: %d cacaoPriceUsd: %.2f\n", pool, poolInfo.RuneDepth, depth, poolInfo.AssetDepth, cacaoPrice)
		}
	}

	fmt.Fprintf(resp, "\n\ncacaoPriceUSD: %v", CacaoPriceUSD())
}
