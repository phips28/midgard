package record

import "github.com/rs/zerolog/log"

const ChainIDMainnet202303 = "mayachain-mainnet-v1"

func loadMainnet202104Corrections(chainID string) {
	if chainID == ChainIDMainnet202303 {
		log.Info().Msgf(
			"Loading corrections for chaosnet started on 2023-03-13 id: %s",
			chainID)
		loadMainnetCorrectGenesisPool()
		loadMainnetCorrectGenesisStake()
		loadMissingPoolBalanceChange()
		loadMissingPoolStatus()
	}
}

func loadMainnetCorrectGenesisPool() {
	AdditionalEvents.Add(1, func(meta *Metadata) {
		stake := Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte(MidgardBalanceCorrectionAddress),
				AssetE8:    0,
				RuneE8:     6,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 6,
		}
		Recorder.OnStake(&stake, meta)
	})
}

func loadMainnetCorrectGenesisStake() {
	AdditionalEvents.Add(2, func(meta *Metadata) {
		stake := Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya1v7gqc98d7d2sugsw5p4pshv0mm24mfmzgmj64n"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)

		stake = Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya1q3jj8n8pkvl2kjv3pajdyju4hp92cmxnadknd2"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)

		stake = Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya10sy79jhw9hw9sqwdgu0k4mw4qawzl7czewzs47"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)

		stake = Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya1gv85v0jvc0rsjunku3qxempax6kmrg5jqh8vmg"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)

		stake = Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya1vm43yk3jq0evzn2u6a97mh2k9x4xf5mzp62g23"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)

		stake = Stake{
			AddBase: AddBase{
				Pool:       []byte("BTC.BTC"),
				AssetTx:    []byte(MidgardBalanceCorrectionAddress),
				AssetChain: []byte("BTC"),
				AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
				RuneAddr:   []byte("maya1g8dzs4ywxhf8hynaddw4mhwzlwzjfccakkfch7"),
				AssetE8:    0,
				RuneE8:     1000000000000,
				RuneTx:     []byte(MidgardBalanceCorrectionAddress),
				RuneChain:  []byte("THOR"),
			},
			StakeUnits: 1000000000000,
		}
		Recorder.OnStake(&stake, meta)
	})
}
func loadMissingPoolBalanceChange() {
	AdditionalEvents.Add(528070, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:   []byte("BTC.BTC"),
			RuneAmt: 1_501_734_01773759,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)

		poolBalanceChange = PoolBalanceChange{
			Asset:   []byte("ETH.ETH"),
			RuneAmt: 4736_57580023,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)

		poolBalanceChange = PoolBalanceChange{
			Asset:   []byte("THOR.RUNE"),
			RuneAmt: 211_877_34242261,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
	AdditionalEvents.Add(547276, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("BTC.BTC"),
			AssetAmt: 4,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})

	AdditionalEvents.Add(546765, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("ETH.ETH"),
			AssetAmt: 7,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
	AdditionalEvents.Add(546474, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48"),
			AssetAmt: 3,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
	AdditionalEvents.Add(546709, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7"),
			AssetAmt: 4,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
	AdditionalEvents.Add(547311, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("THOR.RUNE"),
			AssetAmt: 12,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
	//Fix v107 migration missed events
	AdditionalEvents.Add(3170000, func(meta *Metadata) {
		poolBalanceChange := PoolBalanceChange{
			Asset:    []byte("DASH.DASH"),
			AssetAmt: 43832476664,
			AssetAdd: true,
		}
		Recorder.OnPoolBalanceChange(&poolBalanceChange, meta)
	})
}
func loadMissingPoolStatus() {
	AdditionalEvents.Add(4374, func(meta *Metadata) {
		pool := Pool{
			Asset:  []byte("BTC.BTC"),
			Status: []byte("Available"),
		}
		Recorder.OnPool(&pool, meta)
	})
}
