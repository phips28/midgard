package record

import "github.com/rs/zerolog/log"

func loadStagenetCorrections(chainID string) {
	if chainID == "mayachain-stagenet-v1" {
		log.Info().Msgf(
			"Loading corrections for: %s",
			chainID)
		AdditionalEvents.Add(1, func(meta *Metadata) {
			stake := Stake{
				AddBase: AddBase{
					Pool:       []byte("BTC.BTC"),
					AssetTx:    []byte(MidgardBalanceCorrectionAddress),
					AssetChain: []byte("BTC"),
					AssetAddr:  []byte(MidgardBalanceCorrectionAddress),
					RuneAddr:   []byte(MidgardBalanceCorrectionAddress),
					AssetE8:    0,
					RuneE8:     71000000000008,
					RuneTx:     []byte(MidgardBalanceCorrectionAddress),
					RuneChain:  []byte("THOR"),
				},
				StakeUnits: 71000000000008,
			}
			Recorder.OnStake(&stake, meta)
		})

	}
}
