stages:
  - test
  - build
  - integration

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"

lint:
  stage: test
  image: golangci/golangci-lint:v1.46
  script:
    - golangci-lint run -v

test:
  stage: test
  image: golang:1.18-alpine
  services:
    - docker:20.10.14-dind
  variables:
    DB_HOST: docker
    DB_PORT: "5433"
  script:
    - apk add --no-cache docker docker-compose git make musl-dev gcc
    - until docker info; do echo "Waiting for docker..."; sleep 1; done
    - docker-compose up -d pgtest
    - go version
    - go test -v -p 1 ./...

smoke:
  stage: integration
  image: docker:stable
  tags:
    - mayachain
  services:
    - docker:20.10.14-dind
  artifacts:
    when: on_failure
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - ./logs/
  except:
    - schedules
  variables:
    MIDGARD_REPO: https://gitlab.com/mayachain/midgard.git
    MIDGARD_IMAGE: registry.gitlab.com/mayachain/midgard:develop
    MAYANODE_REPO: https://gitlab.com/mayachain/mayanode.git
    MAYANODE_IMAGE: registry.gitlab.com/mayachain/mayanode:develop
    BLOCK_TIME: 0.8s
    MAYA_BLOCK_TIME: 0.8s
    BLOCK_SCANNER_BACKOFF: 0.8s
    ETH_BLOCK_TIME: "1"
  before_script:
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27384#note_497228752
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - apk -U add make git jq curl protoc
    - |
      PLUGIN="$HOME/.docker/cli-plugins/docker-compose"
      mkdir -p $(dirname $PLUGIN)
      wget https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-linux-x86_64 -O $PLUGIN
      chmod +x $PLUGIN
    - |
      PLUGIN="$HOME/.docker/cli-plugins/docker-buildx"
      mkdir -p $(dirname $PLUGIN)
      wget https://github.com/docker/buildx/releases/download/v0.10.4/buildx-v0.10.4.linux-amd64 -O $PLUGIN
      chmod +x $PLUGIN
    - IMAGE_NAME=$MIDGARD_IMAGE make build
    - git clone --single-branch -b develop $MAYANODE_REPO && docker pull $MAYANODE_IMAGE && docker tag $MAYANODE_IMAGE registry.gitlab.com/mayachain/mayanode:mocknet
  script:
    - cd ./mayanode && make openapi smoke-protob-docker smoke-remote-ci
  after_script:
    - ./mayanode/scripts/docker_logs.sh

build:
  stage: build
  image: docker:stable
  only:
    - master
    - develop
    - tags
  services:
    - docker:20.10.14-dind
  before_script:
    - |
      for i in $(seq 1 30)
      do
          docker info && break
          echo "Waiting for docker to start"
          sleep 1s
      done
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA --tag $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - docker push $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
